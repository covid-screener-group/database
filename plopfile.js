const { capitalCase } = require('change-case');
const docPathCreator = require('./plop-utils/functions/doc-path-creator');
const dateToday = require('./plop-utils/functions/date-today');
const filenameCreator = require('./plop-utils/functions/filename-creator');
const filenamePathCreator = require('./plop-utils/functions/filename-path-creator');
const addLicenseSnippet = require('./plop-utils/functions/add-license-snippet');

module.exports = plop => {
  // general helpers
  plop.setHelper('docPathCreator', docPathCreator);
  plop.setHelper('capitalCase', capitalCase);
  plop.setHelper('dateToday', dateToday);
  plop.setHelper('filenameCreator', filenameCreator);
  plop.setHelper('filenamePathCreator', filenamePathCreator);
  plop.setHelper('addLicenseSnippet', addLicenseSnippet);

  // generator setting
  plop.setGenerator('create:file', {
    description: 'Create a new file',
    prompts: require('./plop-utils/prompts/add-file'),
    actions: require('./plop-utils/actions/add-file')
  });

  plop.setGenerator('create:doc', {
    description: 'Add a document page',
    prompts: require('./plop-utils/prompts/add-document-page'),
    actions: require('./plop-utils/actions/add-document-page')
  });
};
