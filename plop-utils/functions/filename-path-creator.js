/**
 * @return {String}
 */
module.exports = function () {
  // @ts-ignore
  const { path } = this;

  return path ? `${path}/` : '';
};
