const { paramCase } = require('change-case');

/**
 * @return {String}
 */
module.exports = function () {
  // @ts-ignore
  const { pageIndex, pageTitle, path } = this;

  return `${paramCase(pageTitle)}${pageIndex || !path ? '/_index.md' : '.md'}`;
};
