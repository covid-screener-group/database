const getDirectoryAndSubdirectory = require('../functions/get-directory-and-sub-directory');

module.exports = [
  {
    type: 'input',
    name: 'pageTitle',
    message: 'Page Title',
    /**
     * @param {String} value
     */
    validate: (value) => {
      if ((/.+/).test(value) && value !== null) { return true; }
      return 'Page Title is Required';
    }
  },
  {
    type: 'list',
    name: 'path',
    message: 'What would be the path (if it is a subchapter) of the page relative to the docs folder (leave blank if it is a main chapter of doc)',
    choices: () => {
      const choices = getDirectoryAndSubdirectory('docs');
      choices.push('(Root)');
      return choices.map(item => item.replace('docs/', ''));
    },
    /**
     * @param {String} value
     * @return {String | null}
     */
    filter: value => {
      return value === '(Root)' ? null : value;
    }
  },
  {
    type: 'confirm',
    name: 'pageIndex',
    message: 'Will this be a folder index?',
    default: true,
    /**
     * @param {{ path: String }} data
     */
    when: data => {
      const { path } = data;
      return path;
    }
  },
  {
    type: 'input',
    name: 'weight',
    default: 0,
    message: 'What would be the order of this page related to others? (will be multiplied to 10 after)',
    /**
     * @param {Number} value
     * @return {String | Boolean}
     */
    validate: value => {
      if (isNaN(value)) {
        return 'Please put an integer';
      }
      return true;
    },
    /**
     * @param {String} value
     * @return {Number}
     */
    filter: value => parseInt(value) * 10
  }
];
