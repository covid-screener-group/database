// @ts-ignore
const isInvalidPath = require('is-invalid-path');

module.exports = [
  {
    type: 'input',
    name: 'path',
    message: 'Put the relative path folder where you want to put the new file (leave empty if it should be put in root) [Ex. path/to/folder]?',
    /**
     * @param {String} value
     * @return {String | Boolean}
     */
    validate: (value) => {
      if (((/.+/).test(value) && !isInvalidPath(value, { file: false })) || value === '') { return true; }
      return 'Path is required';
    }
  },
  {
    type: 'list',
    name: 'fileType',
    message: 'What is the file type',
    choices: [
      'Javascript: js',
      'Typescript: ts',
      'Python: py',
      'Markdown: md',
      'HTML: html',
      'CSS: css',
      'SCSS: scss',
      'SASS: sass',
      'Stylus: styl',
      'XML: xml',
      'YAML: yml',
      'JSON: json',
      'TOML: toml',
      '(None)'
    ],
    /**
     * @param {String} value
     * @return {String}
     */
    filter: value => {
      if (value === '(None)') return '';
      return `.${value.split(': ').pop()}`;
    }
  },
  {
    type: 'input',
    name: 'fileName',
    message: 'What is the name of the file (if you did not add a file type, add the file type here)',
    /**
     * @param {String} value
     */
    validate: (value) => {
      if ((/.+/).test(value) || value === '') { return true; }
      return 'Path is required';
    }
  }
];
